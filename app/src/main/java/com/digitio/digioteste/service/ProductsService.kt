package com.digitio.digioteste.service

import com.digitio.digioteste.dto.ProductsDTO
import com.digitio.digioteste.interfaces.ServiceCallback
import com.digitio.digioteste.manager.RestManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object ProductsService {

    fun getProducts(callback: ServiceCallback<ProductsDTO?, Exception>) {
        val service = RestManager.retrofit.create<ProductsApi>(ProductsApi::class.java)
        val call = service.getProducts()
        call.enqueue(object: Callback<ProductsDTO> {
            override fun onFailure(call: Call<ProductsDTO>, t: Throwable) {
                callback.onError(Exception(t))
            }

            override fun onResponse(call: Call<ProductsDTO>, response: Response<ProductsDTO>) {
                callback.onSuccess(response.body())
            }

        })

    }
}