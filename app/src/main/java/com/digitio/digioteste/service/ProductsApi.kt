package com.digitio.digioteste.service

import com.digitio.digioteste.dto.ProductsDTO
import retrofit2.Call
import retrofit2.http.GET

interface ProductsApi {

    @GET("products")
    fun getProducts(): Call<ProductsDTO>

}