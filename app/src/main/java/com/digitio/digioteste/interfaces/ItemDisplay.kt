package com.digitio.digioteste.interfaces

interface ItemDisplay {

    var name: String?
    var description: String?
    fun getImageUrl(): String

}