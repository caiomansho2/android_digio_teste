package com.digitio.digioteste.interfaces

interface ServiceCallback<T, K> {

    fun onSuccess(t: T)
    fun onError(k: K)

}