package com.digitio.digioteste

import android.graphics.Color
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.Observable
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.SnapHelper
import com.afollestad.materialdialogs.MaterialDialog
import com.digitio.digioteste.adapters.DisplayAdapter
import com.digitio.digioteste.adapters.ProductAdapter
import com.digitio.digioteste.databinding.ActivityMainBinding
import com.digitio.digioteste.viewmodel.MainViewModel
import com.digitio.digioteste.viewmodel.MainViewModelFactory
import com.digitio.digioteste.widget.ProgressDialog
import com.digitio.digioteste.widget.bindImageUrl


class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var progressDialog: MaterialDialog
    private lateinit var mainViewModel: MainViewModel
    private lateinit var spotLightDisplayAdapter: DisplayAdapter
    private lateinit var productsDisplayAdapter: ProductAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.lifecycleOwner = this

        setSupportActionBar(binding.mainToolbar)
        supportActionBar?.title = ""
        progressDialog = ProgressDialog.create(this)
        setupText()
        setupRecyclers()
        setupViewModels()
    }

    private fun setupText() {
        val cashTitle: Spannable = SpannableString(getText(R.string.main_cash))
        cashTitle.setSpan(
            ForegroundColorSpan(Color.LTGRAY),
            6,
            10,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
        binding.mainSpotlightTextView.text = cashTitle
    }

    private fun setupRecyclers() {
        spotLightDisplayAdapter = DisplayAdapter(arrayListOf())
        productsDisplayAdapter = ProductAdapter(arrayListOf())
        binding.mainSpotlightRecycler.run {
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = LinearLayoutManager(this@MainActivity, LinearLayoutManager.HORIZONTAL, false)

            // specify an viewAdapter (see also next example)
            adapter = spotLightDisplayAdapter

        }
        val helper: SnapHelper = LinearSnapHelper()
        helper.attachToRecyclerView(binding.mainSpotlightRecycler)

        binding.mainProductsRecycler.run {
            setHasFixedSize(true)

            // use a linear layout manager
            layoutManager = LinearLayoutManager(this@MainActivity, LinearLayoutManager.HORIZONTAL, false)

            // specify an viewAdapter (see also next example)
            adapter = productsDisplayAdapter

        }
    }

    private fun setupViewModels() {
        mainViewModel = ViewModelProvider(
            this,
            MainViewModelFactory(this)
        ).get(MainViewModel::class.java)
        val loadingCallback = object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(observable: Observable, i: Int) {
                if(mainViewModel.loading.get()) {
                    progressDialog.show()
                } else {
                    progressDialog.dismiss()
                }
            }
        }
        mainViewModel.loading.addOnPropertyChangedCallback(loadingCallback)
        mainViewModel.products.observe(this, Observer {

            binding.mainCashImageView.bindImageUrl(it.cash)

            spotLightDisplayAdapter.addItems(it.spotlight)
            productsDisplayAdapter.addItems(it.products)

        })

        mainViewModel.getProducts()

    }
}