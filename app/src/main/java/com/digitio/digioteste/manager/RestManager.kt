package com.digitio.digioteste.manager

import com.digitio.digioteste.BuildConfig.BASE_SERVICE_URL
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import java.util.concurrent.TimeUnit


object RestManager {

    val retrofit: Retrofit = Retrofit.Builder()
        .addConverterFactory(JacksonConverterFactory.create())
        .baseUrl(BASE_SERVICE_URL)
        .client(buildOkHttp().build())
        .build()

    private fun buildOkHttp(): OkHttpClient.Builder {
        return OkHttpClient.Builder().readTimeout(120, TimeUnit.SECONDS)
            .connectTimeout(120, TimeUnit.SECONDS)
            .writeTimeout(120, TimeUnit.SECONDS)
    }

}