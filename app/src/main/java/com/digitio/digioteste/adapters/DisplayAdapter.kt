package com.digitio.digioteste.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.digitio.digioteste.databinding.ListItemDisplayBinding
import com.digitio.digioteste.interfaces.ItemDisplay
import com.digitio.digioteste.widget.bindImageUrl

class DisplayAdapter(private var mDataSet: List<ItemDisplay>) :
    RecyclerView.Adapter<DisplayAdapter.DisplayViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DisplayViewHolder {
        val binding = ListItemDisplayBinding.inflate(LayoutInflater.from(parent.context))
        return DisplayViewHolder(binding)
    }

    override fun onBindViewHolder(holder: DisplayViewHolder, position: Int) {
        val spotlight = mDataSet[position]
        holder.binding.listItemDisplayImageView.bindImageUrl(spotlight)
    }

    override fun getItemCount() = mDataSet.size

    fun addItems(mDataSet: List<ItemDisplay>) {
        this.mDataSet = mDataSet
        notifyDataSetChanged()
    }

    class DisplayViewHolder(val binding: ListItemDisplayBinding) : RecyclerView.ViewHolder(binding.root)

}