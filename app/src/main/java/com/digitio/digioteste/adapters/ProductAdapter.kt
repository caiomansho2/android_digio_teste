package com.digitio.digioteste.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.digitio.digioteste.databinding.ListItemDisplayBinding
import com.digitio.digioteste.databinding.ListItemProductBinding
import com.digitio.digioteste.interfaces.ItemDisplay
import com.digitio.digioteste.widget.bindImageUrl

class ProductAdapter(private var mDataSet: List<ItemDisplay>) :
    RecyclerView.Adapter<ProductAdapter.ProductViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val binding = ListItemProductBinding.inflate(LayoutInflater.from(parent.context))
        return ProductViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        val product = mDataSet[position]
        holder.binding.listItemProductImageView.bindImageUrl(product)
    }

    override fun getItemCount() = mDataSet.size

    fun addItems(mDataSet: List<ItemDisplay>) {
        this.mDataSet = mDataSet
        notifyDataSetChanged()
    }

    class ProductViewHolder(val binding: ListItemProductBinding) : RecyclerView.ViewHolder(binding.root)

}