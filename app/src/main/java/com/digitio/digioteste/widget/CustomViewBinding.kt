package com.digitio.digioteste.widget

import android.widget.ImageView
import android.widget.Toast
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.digitio.digioteste.interfaces.ItemDisplay

@BindingAdapter(value = ["setImageUrl"])
fun ImageView.bindImageUrl(itemDisplay: ItemDisplay) {
    val url = itemDisplay.getImageUrl()
    if (url.isNotBlank()) {
        Glide.with(this).load(url).into(this)
        itemDisplay.name.let {bindName ->
            setOnClickListener {
                Toast.makeText(context, "Clicked $bindName", Toast.LENGTH_SHORT).show()
            }

        }
        itemDisplay.description?.let {description ->
            contentDescription = description
        }
    }
}