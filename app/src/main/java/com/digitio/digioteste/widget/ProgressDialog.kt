package com.digitio.digioteste.widget

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Gravity
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.digitio.digioteste.R

object ProgressDialog {

    fun create(context: Context): MaterialDialog {
        val materialDialog = MaterialDialog(context)
            .cancelOnTouchOutside(false)
            .customView(R.layout.layout_progress_dialog)

        materialDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        materialDialog.window?.setGravity(Gravity.CENTER)
        materialDialog.window?.setDimAmount(0f)
        return materialDialog

    }

}