package com.digitio.digioteste

import android.app.Application
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule

class DigioApplication : Application() {

    companion object{
        lateinit var context: Application

    }

    override fun onCreate() {
        super.onCreate()
        context = this
        ObjectMapper().registerModule(KotlinModule())
    }

}