package com.digitio.digioteste.viewmodel

import android.content.Context
import android.widget.Toast
import androidx.databinding.ObservableBoolean
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.digitio.digioteste.R
import com.digitio.digioteste.dto.ProductsDTO
import com.digitio.digioteste.interfaces.ServiceCallback
import com.digitio.digioteste.service.ProductsService


class MainViewModel(val context: Context) : ViewModel() {
    var products = MutableLiveData<ProductsDTO>()
    var loading = ObservableBoolean(false)

    fun getProducts() {
        loading.set(true)
        ProductsService.getProducts(object: ServiceCallback<ProductsDTO?, Exception> {
            override fun onSuccess(data: ProductsDTO?) {
                loading.set(false)
                data?.let {
                    products.value = it
                } ?: kotlin.run {
                    Toast.makeText(context, R.string.error_generic, Toast.LENGTH_LONG).show()
                }
            }

            override fun onError(k: Exception) {
                loading.set(false)
                Toast.makeText(context, R.string.error_generic, Toast.LENGTH_LONG).show()
            }

        })
    }
}