package com.digitio.digioteste.dto

import android.os.Parcelable
import com.digitio.digioteste.interfaces.ItemDisplay
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonIgnoreProperties(ignoreUnknown = true)
data class ProductDTO (

    @JsonProperty("name")
    override var name: String?,

    @JsonProperty("imageURL")
    var imageURL: String,

    @JsonProperty("description")
    override var description: String?

): Parcelable, ItemDisplay {

    override fun getImageUrl(): String {
        return imageURL
    }

}