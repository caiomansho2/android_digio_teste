package com.digitio.digioteste.dto

import android.os.Parcelable
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonIgnoreProperties(ignoreUnknown = true)
data class ProductsDTO(

    @JsonProperty("spotlight")
    var spotlight: List<BannerDTO>,

    @JsonProperty("products")
    var products: List<ProductDTO>,

    @JsonProperty("cash")
    var cash: BannerDTO

): Parcelable
