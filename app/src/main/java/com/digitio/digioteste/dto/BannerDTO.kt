package com.digitio.digioteste.dto

import android.os.Parcelable
import com.digitio.digioteste.interfaces.ItemDisplay
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import kotlinx.android.parcel.Parcelize

@JsonIgnoreProperties(ignoreUnknown = true)
@Parcelize
data class BannerDTO(

    @JsonProperty("name")
    override var name: String?,

    @JsonProperty("bannerURL")
    var bannerURL: String,

    @JsonProperty("description")
    override var description: String?

): Parcelable, ItemDisplay {

    override fun getImageUrl(): String {
        return bannerURL
    }

}